import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterentrepriseService {

  constructor(private http:HttpClient) { }


  registerentreprise(requestregisterentreprise:any){
     return this.http.post(`${environment.baseurl}/users/entreprise/registerentreprise`,requestregisterentreprise)
   }

}
