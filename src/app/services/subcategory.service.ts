import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {

  constructor(private http:HttpClient) { }
  getallsubcategory(){
    return this.http.get(`${environment.baseurl}/users/subcategory/all`)
  }

  deletesubcategory(idsubcategory:any){
    return this.http.delete(`${environment.baseurl}/users/subcategory/delete/${idsubcategory}`)
  }
  
  createsubcategory(subcategory:any,idcategory:any){
    return this.http.post(`${environment.baseurl}/users/subcategory/save/${idcategory}`,subcategory)
  }
}
