import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http:HttpClient) { 

  }
  
  getorder(){
    return this.http.get(`${environment.baseurl}/users/commande/all`)
  }
  getlivraison(){
    return this.http.get(`${environment.baseurl}/users/livraison/all`)
  }
}
