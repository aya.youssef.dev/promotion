import { TestBed } from '@angular/core/testing';

import { ProfilsettingsService } from './profilsettings.service';

describe('ProfilsettingsService', () => {
  let service: ProfilsettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfilsettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
