import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfilserviceService {

  constructor(private http:HttpClient) { 

  }
  
  getoneprofil(id:any){
    return this.http.get(`${environment.baseurl}/users/client/getone/${id}`)
  }
}
