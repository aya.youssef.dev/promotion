import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListproductService {

  constructor(private http:HttpClient) {
 
  }

  getalllistproduct(){
   return this.http.get(`${environment.baseurl}/users/article/all`)
 }
 deletelistproduct(idlistproduct:any){
  return this.http.delete(`${environment.baseurl}/users/article/delete/${idlistproduct}`)
}
getoneproduct(id:any){
  return this.http.get(`${environment.baseurl}/users/article/getone/${id}`)
}



createproduct(product:any,idsubcategory:any,identreprise:any){
  return this.http.post(`${environment.baseurl}/users/article/save/${idsubcategory}/${identreprise}`,product)
}

createproductclient(idclient:any,idsubcategory:any,product:any){
  return this.http.post(`${environment.baseurl}/users/article/save2/${idclient}/${idsubcategory}`,product)
}
updateproductclient(id:any,idclient:any,idsubcategory:any,product:any){
  return this.http.put(`${environment.baseurl}/users/article/update/${id}/${idclient}/${idsubcategory}`,product)
}
}
