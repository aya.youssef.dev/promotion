import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {
 

  constructor(private http:HttpClient) {
 
   }

   getallentreprise(){
    return this.http.get(`${environment.baseurl}/users/entreprise/all`)
  }
  deleteentreprise(identreprise:any){
    return this.http.delete(`${environment.baseurl}/users/entreprise/delete/${identreprise}`)
  }

  getoneentreprise(id:any){
    return this.http.get(`${environment.baseurl}/users/entreprise/getone/${id}`)
  }
}
