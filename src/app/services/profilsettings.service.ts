import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfilsettingsService {

  constructor(private http:HttpClient) {

   }

   updateprofil(id:any,client:any){
     return this.http.put(`${environment.baseurl}/users/client/update/${id}`,client)
   }

   getoneclient(id:any){
    return this.http.get(`${environment.baseurl}/users/client/getone/${id}`)
  }

  updateimageprofil(id:any,client:any){
    return this.http.put(`${environment.baseurl}/users/client/modifimage/${id}`,client)
  }

}
