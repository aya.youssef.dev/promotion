import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterclientService {

  constructor(private http:HttpClient) { }


  registerclient(requestregisterclient:any){
     return this.http.post(`${environment.baseurl}/users/client/registerclient`,requestregisterclient)
   }

 

}
