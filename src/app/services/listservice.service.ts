import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListserviceService {
 

  constructor(private http:HttpClient) {
 
   }

   getalllistserviceservice(){
    return this.http.get(`${environment.baseurl}/users/servvice/all`)
  }
  deletelistservice(idlistservice:any){
    return this.http.delete(`${environment.baseurl}/users/servvice/delete/${idlistservice}`)
  }
  getonelistservice(id:any){
    return this.http.get(`${environment.baseurl}/users/servvice/getone/${id}`)
  }

  createservice(listservice:any,identreprise:any,idsubcategory:any){
    return this.http.post(`${environment.baseurl}/users/servvice/save/${idsubcategory}/${identreprise}`,listservice)
  }
}