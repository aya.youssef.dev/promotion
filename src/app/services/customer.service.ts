import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
 

  constructor(private http:HttpClient) {
 
   }

   getallcustomer(){
    return this.http.get(`${environment.baseurl}/users/client/all`)
  }
  deletecustomer(idcustomer:any){
    return this.http.delete(`${environment.baseurl}/users/client/delete/${idcustomer}`)
  }
}
