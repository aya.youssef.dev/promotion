import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListserviceService } from 'src/app/services/listservice.service';
import Swal from 'sweetalert2';
import { SubcategoryService } from '../services/subcategory.service';

@Component({
  selector: 'app-listservice',
  templateUrl: './listservice.component.html',
  styleUrls: ['./listservice.component.css']
})
export class ListserviceComponent implements OnInit {

  listsubcategory:any
  listservice: any;
  serviceForm: FormGroup;
  fileToUpload:Array<File>=[]
  userconnect=JSON.parse(localStorage.getItem('userconnect')!)
p:number=1
  constructor(private apilistservice:ListserviceService,private formBuilder: FormBuilder,private apidata:SubcategoryService) { }

  ngOnInit(): void {

    this.serviceForm = this.formBuilder.group({
      nameservice: ['', Validators.required],
      prixservice: ['', Validators.required],
      descriptionservice: ['', Validators.required],
      adresse: ['', Validators.required],
      tel: ['', Validators.required],
      idsubcategory: ['', Validators.required],
  });
    this.getalllistservice()
    this.getalllistsubcategory()
  }
 
getalllistservice(){
  this.apilistservice.getalllistserviceservice().subscribe((res:any)=>{
console.log(res)
this.listservice=res
console.log("list service",this.listservice)
  })
}
deletelistservice(id:any){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {

      this.apilistservice.deletelistservice(id).subscribe((res:any)=>{
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      })
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
      this.getalllistservice()
    }
  })
}
handleFileInput(files: any){
  this.fileToUpload=<Array<File>>files.target.files;
  console.log(this.fileToUpload);
}

 createservice(){

  let formdata=new FormData();
  formdata.append("nameservice",this.serviceForm.value.nameservice);
  formdata.append("prixservice",this.serviceForm.value.prixservice);
  formdata.append("descriptionservice",this.serviceForm.value.descriptionservice);
  formdata.append("adresse",this.serviceForm.value.adresse);
  formdata.append("tel",this.serviceForm.value.tel);
  formdata.append("file",this.fileToUpload[0]);

  this.apilistservice.createservice(formdata,this.userconnect.id,this.serviceForm.value.idsubcategory).subscribe((res:any)=>{
    console.log(res)
    Swal.fire(' service added !!')
    
  this.getalllistservice()
   })
  }

  getalllistsubcategory(){
    this.apidata.getallsubcategory().subscribe((res:any)=>{
    console.log(res)
    this.listsubcategory=res
    console.log("list subcategory",this.listsubcategory)
  })
      }

 }



