import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListserviceService } from 'src/app/services/listservice.service';

@Component({
  selector: 'app-detaillistservice',
  templateUrl: './detaillistservice.component.html',
  styleUrls: ['./detaillistservice.component.css']
})
export class DetaillistserviceComponent implements OnInit {

  id=this.activeroute.snapshot.params['id']
  listservice:any;
    constructor(private activeroute:ActivatedRoute,private apilistservice:ListserviceService) { }
  
    ngOnInit(): void {
      // console.log(this.id)
      console.log(this.id)
      this.getonelistservice();
    }
    getonelistservice(){
      this.apilistservice.getonelistservice(this.id).subscribe((res:any)=>{
    console.log(res)
    this.listservice=res
    console.log("list service ",this.listservice)
      })
    }

}

