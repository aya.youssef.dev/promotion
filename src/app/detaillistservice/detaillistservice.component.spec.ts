import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaillistserviceComponent } from './detaillistservice.component';

describe('DetaillistserviceComponent', () => {
  let component: DetaillistserviceComponent;
  let fixture: ComponentFixture<DetaillistserviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetaillistserviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaillistserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
