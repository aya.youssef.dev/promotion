import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterentrepriseService } from '../services/registerentreprise.service';

@Component({
  selector: 'app-registerentreprise',
  templateUrl: './registerentreprise.component.html',
  styleUrls: ['./registerentreprise.component.css']
})
export class RegisterentrepriseComponent implements OnInit {

  registerentrepriseForm: FormGroup;
  fileToUpload:Array<File>=[]
     constructor(private apiregisterentreprise:RegisterentrepriseService,private formBuilder: FormBuilder,private route:Router) { }
  
    ngOnInit(): void {
    
      
      this.registerentrepriseForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        username: ['', Validators.required],
        adresse: ['', Validators.required],
        tel: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', Validators.required],
     
       
     });
  }
   
       
  handleFileInput(files: any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }
  
  onsubmit(){
    let formdata=new FormData();
    formdata.append("fistName",this.registerentrepriseForm.value.fistName);
    formdata.append("lastName",this.registerentrepriseForm.value.lastName);
    formdata.append("username",this.registerentrepriseForm.value.username);
    formdata.append("adresse",this.registerentrepriseForm.value.adresse);
    formdata.append("tel",this.registerentrepriseForm.value.tel);
    formdata.append("email",this.registerentrepriseForm.value.email);
    formdata.append("password",this.registerentrepriseForm.value.password);
    formdata.append("file",this.fileToUpload[0]);





   console.log("bonj")
   this.apiregisterentreprise.registerentreprise(formdata).subscribe((res:any)=>{
   console.log(res)
   Swal.fire(' company registred  ')
    this.route.navigateByUrl('/')
  
   })
   }
  

  

}