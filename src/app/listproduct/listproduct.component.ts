import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ListproductService } from '../services/listproduct.service';
import { SubcategoryService } from '../services/subcategory.service';

@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css']
})
export class ListproductComponent implements OnInit {
  listproduct:any;
  productForm: FormGroup;
  listsubcategory:any
  fileToUpload:Array<File>=[]
  userconnect=JSON.parse(localStorage.getItem('userconnect')!)
  
    constructor(private apilistproduct:ListproductService,private apidata:SubcategoryService,private formBuilder: FormBuilder) { }
  
    ngOnInit(): void {

      this.getalllistproduct();
      this.getalllistsubcategory();

      this.productForm = this.formBuilder.group({
        name: ['', Validators.required],
        prix: ['', Validators.required],
        description: ['', Validators.required],
        remise: ['', Validators.required],
        quantite: ['', Validators.required],
        idsubcategory: ['', Validators.required],
    });

    }
    handleFileInput(files: any){
      this.fileToUpload=<Array<File>>files.target.files;
      console.log(this.fileToUpload);
    }
    

  getalllistproduct(){
    this.apilistproduct.getalllistproduct().subscribe((res:any)=>{
  console.log(res)
  this.listproduct=res.filter((el:any)=>el.entreprise!=null && el.entreprise.id==this.userconnect.id)
  console.log("list product",this.listproduct)
    })
  }
  deletelistproduct(id:any){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
  
        this.apilistproduct.deletelistproduct(id).subscribe((res:any)=>{
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        })
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        this.getalllistproduct()
      }
    })
  }
  createproduct(){

    let formdata=new FormData();
    formdata.append("name",this.productForm.value.name);
    formdata.append("prix",this.productForm.value.prix);
    formdata.append("description",this.productForm.value.description);
    formdata.append("remise",this.productForm.value.remise);
    formdata.append("quantite",this.productForm.value.quantite);
    formdata.append("file",this.fileToUpload[0]);

    this.apilistproduct.createproduct(formdata,this.productForm.value.idsubcategory,this.userconnect.id).subscribe((res:any)=>{
    console.log(res)
    Swal.fire('product added !!')
    
  
   })
  }
  
getalllistsubcategory(){
  this.apidata.getallsubcategory().subscribe((res:any)=>{
  console.log(res)
  this.listsubcategory=res
  console.log("list subcategory",this.listsubcategory)
})
    }
}
