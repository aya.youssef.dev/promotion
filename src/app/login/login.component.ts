import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  
    constructor(private apilogin:AuthService,private formBuilder: FormBuilder,private route:Router) { }
  
    ngOnInit(): void {
    
      
      this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        
       
    });
  }
   
       

  
  onsubmit(){
    console.log("bonj")
    this.apilogin.login(this.loginForm.value).subscribe((res:any)=>{
    console.log(res)
    if(res.user.enabled==true){
      localStorage.setItem('userconnect',JSON.stringify(res.user))
      localStorage.setItem('token',res.access_token)
      localStorage.setItem("state","0")
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
    })
window.location.href="http://localhost:4200/home"
    }
   }, err=>{
    Swal.fire({
      icon:'error',
      title:'user not found ',
      text:'Login invalid',
      footer:'password invalid'
    })
    console.log(err)
   })
  }
    
  navigation(e:any){
    if(e.target.value=="1"){
      this.route.navigateByUrl("/register")
    }
    else if(e.target.value=="2"){
      this.route.navigateByUrl("/registerentreprise")
    }else{
      this.route.navigateByUrl("/registerclient")
    }
  }
 
  }
  

  
  
