import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailentrepriseComponent } from './components/detailentreprise/detailentreprise.component';
import { DetailproductComponent } from './components/detailproduct/detailproduct.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ListcategoryComponent } from './components/listcategory/listcategory.component';
import { ListcustomerComponent } from './components/listcustomer/listcustomer.component';
import { ListorderComponent } from './components/listorder/listorder.component';
import { ListproductadminComponent } from './components/listproductadmin/listproductadmin.component';
import { ListproductclientComponent } from './components/listproductclient/listproductclient.component';
import { ListsubcategoryComponent } from './components/listsubcategory/listsubcategory.component';
import { DetaillistserviceComponent } from './detaillistservice/detaillistservice.component';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { ListproductComponent } from './listproduct/listproduct.component';
import { ListserviceComponent } from './listservice/listservice.component';
import { LoginComponent } from './login/login.component';
import { ProfilComponent } from './profil/profil.component';
import { ProfilsettingsComponent } from './profilsettings/profilsettings.component';
import { RegisterComponent } from './register/register.component';
import { RegisterclientComponent } from './registerclient/registerclient.component';
import { RegisterentrepriseComponent } from './registerentreprise/registerentreprise.component';


const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'registerentreprise',component:RegisterentrepriseComponent},
  {path:'registerclient',component:RegisterclientComponent},

  {path:'home',component:HomeComponent,children:[
    {path:'',component:LayoutComponent},
    {path:'listcategory',component:ListcategoryComponent},
    {path:'listsubcategory',component:ListsubcategoryComponent},   
    {path:'listcustomer',component:ListcustomerComponent},
    {path:'entreprise',component:EntrepriseComponent},
    {path:'listproduct',component:ListproductComponent},
    {path:'listproductclient',component:ListproductclientComponent},
    {path:'listservice',component:ListserviceComponent},
    {path:'detailproduct/:id',component:DetailproductComponent},
    {path:'detailentreprise/:id',component:DetailentrepriseComponent},
    {path:'detaillistservice/:id',component:DetaillistserviceComponent},
    {path:'profil',component:ProfilComponent},
    {path:'profilsettings',component:ProfilsettingsComponent},
    {path:'order',component:ListorderComponent},
    {path:'listproductAdmin',component:ListproductadminComponent}


  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

