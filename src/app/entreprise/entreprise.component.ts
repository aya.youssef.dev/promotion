import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { EntrepriseService } from '../services/entreprise.service';

@Component({
  selector: 'app-entreprise',
  templateUrl: './entreprise.component.html',
  styleUrls: ['./entreprise.component.css']
})
export class EntrepriseComponent implements OnInit {
p:number=1
search_name:any
  entreprise: any;
  constructor(private apientreprise:EntrepriseService) { }

  ngOnInit(): void {
    this.getallentreprise()
  }
 
getallentreprise(){
  this.apientreprise.getallentreprise().subscribe((res:any)=>{
console.log(res)
this.entreprise=res
console.log("entreprise",this.entreprise)
  })
}
deleteentreprise(id:any){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {

      this.apientreprise.deleteentreprise(id).subscribe((res:any)=>{
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      })
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
      this.getallentreprise()
    }
  })
}

}