import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  fileToUpload:Array<File>=[]
     constructor(private apiregister:RegisterService,private formBuilder: FormBuilder,private route:Router) { }
  
    ngOnInit(): void {
    
      
      this.registerForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        username: ['', Validators.required],
        password: ['', Validators.required],
     
       
     });
  }
   
       
  handleFileInput(files: any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }
  
  onsubmit(){
    let formdata=new FormData();
    formdata.append("lastName",this.registerForm.value.lastName);
    formdata.append("fistName",this.registerForm.value.fistName);
    formdata.append("username",this.registerForm.value.username);
    formdata.append("password",this.registerForm.value.password);
    formdata.append("file",this.fileToUpload[0]);





   console.log("bonj")
   this.apiregister.register(formdata).subscribe((res:any)=>{
   console.log(res)
   Swal.fire(' Admin registred  ')

    this.route.navigateByUrl('/')
  
   })
   }
  

  

}
