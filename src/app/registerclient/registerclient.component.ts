import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterclientService } from '../services/registerclient.service';

@Component({
  selector: 'app-registerclient',
  templateUrl: './registerclient.component.html',
  styleUrls: ['./registerclient.component.css']
})
export class RegisterclientComponent implements OnInit {

  registerclientForm: FormGroup;
  fileToUpload:Array<File>=[]

  userconnect=JSON.parse(localStorage.getItem('userconnect')!)
  
     constructor(private apiregisterclient:RegisterclientService,private formBuilder: FormBuilder,private route:Router) { }
  
    ngOnInit(): void {
    
      
      this.registerclientForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        username: ['', Validators.required],
        adresse: ['', Validators.required],
        tel: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', Validators.required],
     
       
     });
  }
   
       
  handleFileInput(files: any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }

  getoneclient(){
    
  }
  
  onsubmit(){
    let formdata=new FormData();
    formdata.append("firstName",this.registerclientForm.value.firstName);
    formdata.append("lastName",this.registerclientForm.value.lastName);
    formdata.append("username",this.registerclientForm.value.username);
    formdata.append("adresse",this.registerclientForm.value.adresse);
    formdata.append("tel",this.registerclientForm.value.tel);
    formdata.append("password",this.registerclientForm.value.password);
    formdata.append("file",this.fileToUpload[0]);
   this.apiregisterclient.registerclient(formdata).subscribe((res:any)=>{
   console.log(res)
   Swal.fire(' client registred  ')
    this.route.navigateByUrl('/')
  
   })
   }
  

  

}
