import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ProfilsettingsService } from '../services/profilsettings.service';

@Component({
  selector: 'app-profilsettings',
  templateUrl: './profilsettings.component.html',
  styleUrls: ['./profilsettings.component.css']
})
export class ProfilsettingsComponent implements OnInit {
  profilsettingsForm: FormGroup;

  fileToUpload:Array<File>=[]

  userconnect=JSON.parse(localStorage.getItem('userconnect')!)
client:any
  constructor(private apiprofilsettings:ProfilsettingsService,private formBuilder: FormBuilder,private route:Router){}
  ngOnInit(): void {

    this.profilsettingsForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      adresse: ['', Validators.required],
      tel: ['', Validators.required],
      email: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
 
   
     
   });
   this.getoneclient()
  }
 

  getoneclient(){
    this.apiprofilsettings.getoneclient(this.userconnect.id).subscribe((res:any)=>{
      console.log(res)
      this.client=res
      this.profilsettingsForm.patchValue({
        firstName:res.firstName,
        lastName:res.lastName,
        adresse:res.adresse,
        tel:res.tel,
        email:res.email,
        state:res.state,
        city:res.city,
        country:res.country


      })

    })
  }
  onsubmit(){
   

   this.apiprofilsettings.updateprofil(this.userconnect.id,this.profilsettingsForm.value).subscribe((res:any)=>{
   console.log(res)
   Swal.fire(' profil registred  ')
    // this.route.navigateByUrl('/')
  
   })
   }

   handleFileInput(files: any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }

  updateimage(){
    let formdata=new FormData()
    formdata.append("file",this.fileToUpload[0])
    this.apiprofilsettings.updateimageprofil(this.userconnect.id,formdata).subscribe((res:any)=>{
      console.log(res)
      localStorage.setItem("userconnect",JSON.stringify(res))
      window.location.href="http://localhost:4200/home/profilsettings"
  })
}
  
}
