import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilsettingsComponent } from './profilsettings.component';

describe('ProfilsettingsComponent', () => {
  let component: ProfilsettingsComponent;
  let fixture: ComponentFixture<ProfilsettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilsettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilsettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
