import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListproductclientComponent } from './listproductclient.component';

describe('ListproductclientComponent', () => {
  let component: ListproductclientComponent;
  let fixture: ComponentFixture<ListproductclientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListproductclientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListproductclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
