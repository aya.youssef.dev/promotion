import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/services/category.service';
import { ListproductService } from 'src/app/services/listproduct.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listproductclient',
  templateUrl: './listproductclient.component.html',
  styleUrls: ['./listproductclient.component.css']
})
export class ListproductclientComponent implements OnInit {
  listproduct:any
  productForm: FormGroup;
  productFormupdate: FormGroup;
  
  p: number = 1;
  listsubcategory:any;
  category:any
  ca:any
  fileToUpload:Array<File>=[]
  userconnect=JSON.parse(localStorage.getItem('userconnect')!)
  constructor(private apilistproduct:ListproductService,private apidata:SubcategoryService,private apidata1:CategoryService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
     this.getalllistproduct()
     this.getalllistsubcategory();
     this.getallcategory();

     this.productForm = this.formBuilder.group({
      name: ['', Validators.required],
      prix: ['', Validators.required],
      description: ['', Validators.required],
      remise: ['', Validators.required],
      quantite: ['', Validators.required],
      id_subcategory: ['', Validators.required],
      id_client: ['', Validators.required],

  });

  this.productFormupdate = this.formBuilder.group({
    name: ['', Validators.required],
    prix: ['', Validators.required],
    description: ['', Validators.required],
    remise: ['', Validators.required],
    quantite: ['', Validators.required],
    id: ['', Validators.required],
    id_client: ['', Validators.required],
    id_subcategory:['', Validators.required],



});
  }

  
 


  getalllistproduct(){
    this.apilistproduct.getalllistproduct().subscribe((res:any)=>{
  console.log(res)
  this.listproduct=res.filter((el:any)=>el.client!=null && el.client.id==this.userconnect.id)
  console.log("list product",this.listproduct)
    })
  }

  handleFileInput(files: any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }

  createproduct(){


    let formdata=new FormData();
    formdata.append("name",this.productForm.value.name);
    formdata.append("prix",this.productForm.value.prix);
    formdata.append("description",this.productForm.value.description);
    formdata.append("remise",this.productForm.value.remise);
    formdata.append("quantite",this.productForm.value.quantite);
    formdata.append("id_subcategory",this.productForm.value.id_subcategory);
    formdata.append("id_client",this.productForm.value.id_client);


    formdata.append("file",this.fileToUpload[0]);
   console.log("subcategory",this.productForm.value.id_subcategory)
   this.productForm.patchValue({
    id_client:this.userconnect.id,
    id_subcategory:this.productForm.value.id_subcategory
  })
    this.apilistproduct.createproductclient(this.userconnect.id,this.productForm.value.id_subcategory,formdata).subscribe((res:any)=>{
    console.log(res)
    Swal.fire('product added !!')
    
  this.getalllistproduct()
   })
  }
    
getalllistsubcategory(){
  this.apidata.getallsubcategory().subscribe((res:any)=>{
  console.log(res)
  this.listsubcategory=res
  console.log("list subcategory",this.listsubcategory)
})
    }
    deletelistproduct(id:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
    
          this.apilistproduct.deletelistproduct(id).subscribe((res:any)=>{
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          })
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.getalllistproduct()
        }
      })
    }


 open(product:any){

this.productFormupdate.patchValue({
  name:product.name,
  prix:product.prix,
  description: product.description,
  remise: product.remise,
  quantite: product.quantite,
  id:product.id,
  id_client: product.client.id,
  id_subcategory: product.subcategory.id,

})

    }

    updateproduct(){
      this.apilistproduct.updateproductclient(this.productFormupdate.value.id,this.productFormupdate.value.id_client,this.productFormupdate.value. id_subcategory, this.productForm.value).subscribe((res:any)=>{
console.log(res)
this.getalllistproduct()
    })
  }

  getallcategory(){
    this.apidata1.getallcategory().subscribe((res:any)=>{
  console.log(res)
  this.category=res
  console.log("list category",this.category)
    })
  }
  
  getalllistproductcategory(e:any){
    console.log(e.target.value)
    this.apilistproduct.getalllistproduct().subscribe((res:any)=>{
  console.log(res)
  this.listproduct=res.filter((el:any)=>el.client!=null && el.client.id==this.userconnect.id)
  this.listproduct=  this.listproduct.filter((el:any)=>el.subcategory.categorie.name===e.target.value)

  console.log("list product",this.listproduct)
    })
  }
}
