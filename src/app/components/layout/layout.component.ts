import { Component, OnInit } from '@angular/core';
import { ListproductService } from 'src/app/services/listproduct.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  listorder: any
  listlivraison: any
  nbliv: any
  nborder: any
  nbproduct: any
  search_name:any
  listproduct: any
  p:number=1
  constructor(private orderservice: OrderService, private apilistproduct: ListproductService) { }

  ngOnInit(): void {
    this.getorder()
    this.getalllistproduct()
    this.getlivraison()
  }

  getorder() {
    this.orderservice.getorder().subscribe((res: any) => {
      this.listorder = res
      this.nborder = this.listorder.length
      console.log("nbre order", this.nborder)
      console.log("list order", this.listorder)

    })
  }

  getalllistproduct() {
    this.apilistproduct.getalllistproduct().subscribe((res: any) => {
      console.log(res)
      this.listproduct = res
      this.nbproduct = this.listproduct.length
      console.log("list product", this.listproduct)
      console.log("nbre product", this.nbproduct)

    })
  }

  getlivraison() {
    this.orderservice.getlivraison().subscribe((res: any) => {
      this.listlivraison = res
      this.nbliv = this.listlivraison.length
      console.log("nbre liv", this.nbliv)
      console.log("list liv", this.listlivraison)

    })
  }

}
