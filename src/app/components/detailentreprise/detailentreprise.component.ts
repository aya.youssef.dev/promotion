import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EntrepriseService } from 'src/app/services/entreprise.service';

@Component({
  selector: 'app-detailentreprise',
  templateUrl: './detailentreprise.component.html',
  styleUrls: ['./detailentreprise.component.css']
})
export class DetailentrepriseComponent implements OnInit {

  id=this.activeroute.snapshot.params['id']
  entreprise:any;
    constructor(private activeroute:ActivatedRoute,private apientreprise:EntrepriseService) { }
  
    ngOnInit(): void {
      // console.log(this.id)
      console.log(this.id)
      this.getoneentreprise();
    }
    getoneentreprise(){
      this.apientreprise.getoneentreprise(this.id).subscribe((res:any)=>{
    console.log(res)
    this.entreprise=res
    console.log(" entreprise",this.entreprise)
      })
    }

}
