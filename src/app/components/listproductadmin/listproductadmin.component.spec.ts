import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListproductadminComponent } from './listproductadmin.component';

describe('ListproductadminComponent', () => {
  let component: ListproductadminComponent;
  let fixture: ComponentFixture<ListproductadminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListproductadminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListproductadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
