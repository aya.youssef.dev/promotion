import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  userconnect=JSON.parse(localStorage.getItem('userconnect')!)

  constructor(private route:Router) { }

  ngOnInit(): void {
  }


  isadmin(){
  return this.userconnect.role=="ADMIN" ? true : false;
  }

  isclient(){
    return this.userconnect.role=="CLIENT" ? true : false;
    }

    isentreprise(){
      return this.userconnect.role=="ENTREPRISE" ? true : false;
      }


      logout(){
        localStorage.clear()
        this.route.navigateByUrl('/')

      }
}
