import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/services/category.service';

import { SubcategoryService } from 'src/app/services/subcategory.service';
import Swal from 'sweetalert2';
import { ListcategoryComponent } from '../listcategory/listcategory.component';

@Component({
  selector: 'app-listsubcategory',
  templateUrl: './listsubcategory.component.html',
  styleUrls: ['./listsubcategory.component.css']
})
export class ListsubcategoryComponent implements OnInit { 
listsubcategory:any;
subcategoryForm: FormGroup;
listcategory:any
p:number=1
search_name:any
  constructor(private apisubcategory:SubcategoryService,private apidata:CategoryService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
  
    this.getalllistsubcategory()
    this.getallcategory()
    this.subcategoryForm = this.formBuilder.group({
      namesub: ['', Validators.required],
      description: ['', Validators.required],
      id_categorie: ['', Validators.required],
     
  });
}
 
     
 

 
  
  

getalllistsubcategory(){
  this.apisubcategory.getallsubcategory().subscribe((res:any)=>{
console.log(res)
this.listsubcategory=res
console.log("list subcategory",this.listsubcategory)
  })
}
deletesubcategory(id:any){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {

      this.apisubcategory.deletesubcategory(id).subscribe((res:any)=>{
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      })
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
      this.getalllistsubcategory()
    }
  })
}

createsubcategory(){
  this.apisubcategory.createsubcategory(this.subcategoryForm.value,this.subcategoryForm.value.id_categorie).subscribe((res:any)=>{
  console.log(res)
  Swal.fire('subcategory added !!')
  this.getalllistsubcategory()

 })
}

getallcategory(){
  this.apidata.getallcategory().subscribe((res:any)=>{
  console.log(res)
  this.listcategory=res
  console.log("list category",this.listcategory)
})
    }



}
