import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-listorder',
  templateUrl: './listorder.component.html',
  styleUrls: ['./listorder.component.css']
})
export class ListorderComponent implements OnInit {
  listorder: any
  listorderclient:any
  userconnect=JSON.parse(localStorage.getItem('userconnect')!)

  constructor(private orderservice: OrderService) { }

  ngOnInit(): void {
    this.getorder()
    this.getorderclient()
  }
  getorder() {
    this.orderservice.getorder().subscribe((res: any) => {
      this.listorder = res
      console.log("list order", this.listorder)

    })
  }

  isadmin(){
    return this.userconnect.role=="ADMIN" ? true : false;
    }
  
    isclient(){
      return this.userconnect.role=="CLIENT" ? true : false;
      }

      getorderclient() {
        this.orderservice.getorder().subscribe((res: any) => {
          this.listorderclient = res.filter((el:any)=>el.client.id==this.userconnect.id)
          console.log("list order", this.listorderclient)
    
        })
      }
}
