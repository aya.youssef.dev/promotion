import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListproductService } from 'src/app/services/listproduct.service';

@Component({
  selector: 'app-detailproduct',
  templateUrl: './detailproduct.component.html',
  styleUrls: ['./detailproduct.component.css']
})
export class DetailproductComponent implements OnInit {
id=this.activeroute.snapshot.params['id']
product:any;
  constructor(private activeroute:ActivatedRoute,private apiproduct:ListproductService) { }

  ngOnInit(): void {
    // console.log(this.id)
    console.log(this.id)
    this.getoneproduct();
  }
  getoneproduct(){
    this.apiproduct.getoneproduct(this.id).subscribe((res:any)=>{
  console.log(res)
  this.product=res
  console.log(" product",this.product)
    })
  }

}
