import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listcustomer',
  templateUrl: './listcustomer.component.html',
  styleUrls: ['./listcustomer.component.css']
})
export class ListcustomerComponent implements OnInit {

  listsubcategory:any
  listcustomer: any;
  p:number=1
  search_name:any
  constructor(private apicustomer:CustomerService) { }

  ngOnInit(): void {
    this.getalllistcustomer()
  }
 
getalllistcustomer(){
  this.apicustomer.getallcustomer().subscribe((res:any)=>{
console.log(res)
this.listcustomer=res
console.log("list customer",this.listcustomer)
  })
}
deletecustomer(id:any){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {

      this.apicustomer.deletecustomer(id).subscribe((res:any)=>{
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      })
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
      this.getalllistcustomer()
    }
  })
}

}
