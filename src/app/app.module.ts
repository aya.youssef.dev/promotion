import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { LayoutComponent } from './components/layout/layout.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import  {HttpClientModule} from '@angular/common/http';
import { ListcategoryComponent } from './components/listcategory/listcategory.component';
import { ListsubcategoryComponent } from './components/listsubcategory/listsubcategory.component';
import { ListcustomerComponent } from './components/listcustomer/listcustomer.component';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { ListproductComponent } from './listproduct/listproduct.component';
import { ListserviceComponent } from './listservice/listservice.component';
import { DetailproductComponent } from './components/detailproduct/detailproduct.component';
import { DetailentrepriseComponent } from './components/detailentreprise/detailentreprise.component';
import { DetaillistserviceComponent } from './detaillistservice/detaillistservice.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterentrepriseComponent } from './registerentreprise/registerentreprise.component';
import { RegisterclientComponent } from './registerclient/registerclient.component';
import { ProfilComponent } from './profil/profil.component';
import { ProfilsettingsComponent } from './profilsettings/profilsettings.component';
import { ListproductclientComponent } from './components/listproductclient/listproductclient.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { RecherchePipe } from './pipes/recherche.pipe';
import { ListorderComponent } from './components/listorder/listorder.component';
import { ListproductadminComponent } from './components/listproductadmin/listproductadmin.component';
import { Recherche1Pipe } from './pipes/recherche1.pipe';
import { Recherche2Pipe } from './pipes/recherche2.pipe';



@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    LayoutComponent,
    HomeComponent,
    FooterComponent,
    ListcategoryComponent,
    ListsubcategoryComponent,
    ListcustomerComponent,
    EntrepriseComponent,
    ListproductComponent,
    ListserviceComponent,
    DetailproductComponent,
    DetailentrepriseComponent,
    DetaillistserviceComponent,
    LoginComponent,
    RegisterComponent,
    RegisterentrepriseComponent,
    RegisterclientComponent,
    ProfilComponent,
    ProfilsettingsComponent,
    ListproductclientComponent,
    RecherchePipe,
    ListorderComponent,
    ListproductadminComponent,
    Recherche1Pipe,
    Recherche2Pipe,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
   ReactiveFormsModule,
   NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
